import {CustomAgent} from '../models/agent';
import {RandomAgent} from '../models/agent'; //can be removed once you replace it with your logic
import { State, Team, Location } from '../models/model';

/** 
 * GET /agent/nextTurn route to get a move from your agent 
*/
function getNextTurn(req, res) {
    //console.log("got a req here boss", JSON.stringify(req.body));
    const agent = new RandomAgent(new Team(req.body.team.name));
    //TODO: use this one when you implement your logic
    //const agent = new CustomAgent(new Team(req.body.team.name));
    
    let state = new State().fromJSON(req.body.state, new Team(req.body.team.name));
    //console.log(state.toString());
    const turn = agent.getNextTurn(state);
    res.json(turn);
}

/** 
 * GET /agent/placement route to get a initial placements from your agent
*/
function getInitialPlacements(req, res) {
    //console.log("got a req here boss", JSON.stringify(req.body));
    const agent = new RandomAgent(new Team(req.body.team.name));
    //TODO: use this one when you implement your logic
    //const agent = new CustomAgent(new Team(req.body.team.name));
    const otherTeamPlacements = getLocationArrayFromJSON(req.body.otherTeamPlacements);
    const placements = agent.getInitialPlacements(otherTeamPlacements);
    //console.log(JSON.stringify(placements));
    res.json(placements);
}

//used by getInitialPlacements
function getLocationArrayFromJSON(placements: any[]): Location[]{
    if(!placements) return null;
    return placements.map(placement => {
        return new Location(placement.boardIndex)
    })
}

module.exports = { getNextTurn, getInitialPlacements }