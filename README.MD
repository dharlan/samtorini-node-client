# Sample Client for Samtorini
It's written in typescript. Fork it, freak it, host it. 

Edit the `CustomAgent` class in `controllers/models/agent.ts` to implement your bot's logic.

Go into `controllers/routes/agent.ts` and follow instructions to point the server to use your edited CustomAgent class.

Use `tsc -b` to complile the `*.ts` files down to JS, and `tsc -w` to watch for changes and build on updates to the code

Test using `npm test` to ensure it works. (TODO: write better tests)

Host it on a server, and enter the URL on your account page for Samtorini. (TODO: make that website)

# Required Endpoints
In order for your bot to participate in Samtorini matches, it needs to respond to the following requests over port `8080`:

## /nextTurn - POST
The Request Body will be formatted like this:

    {
        "state": {
            "winner": null,
            "board": [{   //the board is an array of tile objects
                "location": { //each tile has a location object
                    "boardIndex": 0 //each location has a boardIndex    
                                    //(see diagram in Data Model section)
                },
                "level": 0 //indicates hwo many buildigns are on the given tile
                           // (0 being no building, and 4 being capped)
            }, {
                "location": {
                    "boardIndex": 1
                },
                "level": 0
            }, {

                "location": {
                    "boardIndex": 2
                },
                "level": 0
            }, 
            
            ... and so on
            
             {
                "location": {
                    "boardIndex": 23
                },
                "level": 0
            }, {
                "location": {
                    "boardIndex": 24
                },
                "level": 0
            }],
            
            "workers": [{ //an array of worker objects
                "team": { //each worker has a team
                    "name": "HEAD"
                },
                "location": { //and a location of the worker
                    "boardIndex": 7
                }
            }, {
                "team": {
                    "name": "HEAD"
                },
                "location": {
                    "boardIndex": 3
                }
            }, {
                "team": {
                    "name": "STOM"
                },
                "location": {
                    "boardIndex": 12
                }
            }, {
                "team": {
                    "name": "STOM"
                },
                "location": {
                    "boardIndex": 19
                }
            }]
        },
        "team": { //this is your agent's team
            "name": "HEAD"
        }
    }

From this data your agent will need to discern what move should next be made for their team. You should return a Turn object with the follwoing format:

    {
        "move": {
            "worker": { //worker object
                "team": {
                    "name": "HEAD"
                },
                "location": {
                    "boardIndex": 17
                }
            },
            "target": { //loaction object where movement will end for the worker after exceuting the move action
                "boardIndex": 23
            }
        },
        "build": {
            "worker": { //worker object
                "team": {
                    "name": "HEAD"
                },
                "location": {
                    "boardIndex": 17
                }
            },
            "target": { //loaction object where buildign will be built
                "boardIndex": 19
            }
        }
    }


## /placements - POST
The request body should look like this:

    {
        "otherTeamPlacements": [{ //an array of Location objects representing where the other team placed their workers (null if you are the first player)
            "boardIndex": 7
        }, {
            "boardIndex": 9
        }],
        "team": { //your team object
            "name": "HEAD"
        }
    }

You just need to return an array of two location objects indicating where you woudl like to place your workers

    [{
        "boardIndex": 13
    }, {
        "boardIndex": 10
    }]


## Data Model

### State
Properties: 
- board: an array of Tile objects
- workers: an array of Worker objects

Functions:
- constructor() - makes a fresh state, no players, no blocks placed
- placeTeam(team:Team, initialPlacements:Location[]) - perform the placement of one team's initial worker placement on the current state instance, throws an excpetion if it fails.
- takeTurn(turn:Turn) - applies a turn obj to the current state instance, throws an error if the turn is not a valid play.
- getWorkerAtLocation(loc:Location) - put in a spot you get the guy - bada bing, bada boom - no guy? you get null! you get zilch! you get nada! get outta here! (but yeah, it will return null if nobody there)
- isTurnValid(turn:Turn):boolean - check out your turns before ya *turn* them in haha, returns true/false (TODO:This should probably verify that the correct team is calling it)
- isMoveValid(move:Move):boolean - checks out a move for ya, no cost, no problem, gives you great boolean answer right back, we don't play around here, except when we're playin sanotrini baby
- isBuildValid(build:Build):boolean - when you wanna check and see if a build is *even legal* for you to do, like any good builder, you go see your lawyer at isBuildValid and Sons Law Firm and they give you a stragiht boolean answer, no questions asked
- isTileOccupied(tile:Tile):boolean - you got a tile from the board, and you got questions. ask the state, *who stay on that tile over yonder?* and state respondeth with a boolean cry that pierceth the heavens
- isPlacementValid(team: Team, intialPlacemnts: Location[]): boolean - it does what it says on the tin folks - just playing by the rules here, can't have any cohabitation on any of the tiles
- isWorkerValid(worker:Worker):boolean - checks the employment records and sees if any other workers occupy the same spot, used when checking placement validity
- isTeamPresent(team:Team):boolean - ay im looking for [TEAM] ya seen em around?  jsut ask those workers ya got and see if they heard of em - boolean response plz
- canTeamMove(team:Team):boolean - can they move? if no, THEN THEY LOSE OOF
- canWorkerMove(worker:Worker):boolean - checks if worker has a legal move available *(DOES NOT CHECK ILLEGAL MOVES. SAMOTRINI.JS IS NOT REPSPONSIBLE FOR ANY WORKER MOVEMENT OUTSIDE OF THE LAW)*
- getPossibleMoveLocationsForWorker(worker:Worker):number[] - returns board indexes for viable (legal) spots on the board that the given worker can go to based on the current state
- canWorkerMove(worker:Worker):boolean - checks if worker has a legal move available *(DOES NOT CHECK ILLEGAL MOVES. SAMOTRINI.JS IS NOT REPSPONSIBLE FOR ANY WORKER MOVEMENT OUTSIDE OF THE LAW)*
- getWorkersForTeam(team:Team):Worker[] - WHO WORKS FOR THIS TEAM? ANSWER ME! THIS THE POLICE, YEAH! BRING EM OUT HERE IN AN ARRAY THIS INSTANT OR ILL SHOOT!
- getTeams(): Team[] - who are the teams? what are the teams? what is a team? 
- toString():string - yeah idk what this one does tbh

### Team
Properties: 
- name: issa string y'all, if you keep it exactly 4 charaters it will look better on toString for the State
- (TODO: Godpowers lol yeah right)

Functions:
- constructor(name:string) - hm hard to imagine how this might work

### Worker
Chose to have the worker hold what team it's on, might not be the best way to do it idk

Properties:
- team: Team, issa team
- location: Locations - WHERE IS HE???

Functions:
- constructor(team: Team, loc: Location) - give the team they on and the place where they start.
- eqauls(w:Worker): boolean - Doc! Give it to be straight, is this worker right here your clone or what? 

### Turn
Made up of a Move action and a Build action. Eventually add god power actions here too (lol how even).

Properties:
- move: Move - every turn needs a move action, this gets checked/enacted first too
- build: Build -required build action that has to happen at the end of the turn

Functions: 
- constructor(movetarget: Location, buildTarget: Location, worker:Worker) - easy way to build yourself a nice turn, just give a Location where you want to move to, a Location you'll want ot build on once you're there, and which builder will be doing the dang thing. (Make sure to check it's validty agaisnt the state tho)

### Action (private)
This is the parent class for the Move and Build classes (and maybe God Power ones lol). Just holds the worker who's doing the task. Not meant to be used by client

Properties:
- worker: Worker - issa worker

### Move
yeah idk this should probably be merged with Build

Properties:
- worker: Worker - (inherited from Action)
- target: Location - where ya going?
Functions:
- constuctor(worker:Worker, loc:Locaiton) - make one at home with two easy ingredients: the worker ya wanna move, and the place they're headin' (make sure to check validtiy w/State)

### Build
yeah idk this should probably be merged with Move

Properties:
- worker: Worker - (inherited from Action)
- target: Location - where ya building?

Functions:
- constuctor(worker:Worker, loc:Locaiton) - so you've moved. now you must build. provide the one who shall build and the location where the build will be built into a building (tho make sure to check validity w/State)

### Tile
It's what makes up the board. 

Properties:
- location: Location - where layeth said tile?
- level: number - 0-4 scale as follows:
  - 0 - No Building - *the fresh start*
  - 1 - First Level Building - *the studio apartment*
  - 2 - Second Level Building - *the bunk bed*
  - 3 - Third Level Building - *the penthouse*
  - 4 - DOME ZONE - DO NOT ENTER

Functions:
- constructor(loc:Location) - Fresh tiles ain't got no buildings, just a location
- canMoveTo(target:Tile) - from this tile, could we get to this other tile, assumign they're from the same state? accounting for levels and everything? boolean answer plz

### Location
Properties:
- boardIndex: the index this location would have in an array representing the game board starting in the top left corner going right like such:

| 0  | 1  | 2  | 3  | 4  |
|----|----|----|----|----|
| 5  | 6  | 7  | 8  | 9  |
| 10 | 11 | 12 | 13 | 14 |
| 15 | 16 | 17 | 18 | 19 |
| 20 | 21 | 22 | 23 | 24 |

Functions:
- constructor(index:number) - you might be able to discern how this constructor works
- isAdjacent(loc:Loaction):boolean - is the given Location around (but not the same as) the current location instance (here around means diaganol and cardinal directions but not looping around to the other side of the map)
- equals(loc:Location):boolean - ARE THEY THE SAAAAAAAAME?
- getX():number - will give you the x coordiante of the location (assuminng lower left corner of board is the origin)
- getY():number - will give you the y coordiante of the location (assuminng lower left corner of board is the origin)

