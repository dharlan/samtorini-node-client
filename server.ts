const express        = require('express');
const bodyParser     = require('body-parser');
const app = express();
const morgan = require('morgan'); //for testing
const port = 8080;
const config = require('config');
const agent = require('./controllers/routes/agent');


//don't show the log when it is test
if(config.util.getEnv('NODE_ENV') !== 'test') {
    //use morgan to log at command line
    app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

//parse application/json and look for raw text                                        
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

app.get("/", (req, res) => res.json({message:"hello! i'ts me! the santorini agent!"}));

app.post("/nextTurn", agent.getNextTurn);
app.post("/placements", agent.getInitialPlacements);


app.listen(port, () => {
    console.log("samtorini agent sending GOOD VIBES over port " + port);
});

module.exports = app; //for testing



