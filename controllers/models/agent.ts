import {Location, State, Team, Turn, Move, Worker } from "./model";

export class Agent{
    team:Team;
    constructor(team:Team){
        this.team = team;
    }

    //Should return an array of two Location objects 
    //reflecting the desired initial placements for the Agent's workers 
    //at the start of the game
    //If choosing placement after opponent, otherTeam will have their initial placements
    //should be overwritten by custom agent
    getInitialPlacements(otherTeam: Location[] = null) : Location[] {
        return null;
    }

    //Given the current board state, the agent should return their next Action
    getNextTurn(currentState: State):Turn{
        return null;
    }
}

export class CustomAgent implements Agent{
    //keep this 
    team:Team;
    constructor(team:Team){
        this.team = team;
    }

    /*
    asks the agent where it wants to place workers at the start of the game
    if the agent is not going first, the locations of the other player's workers will be passed in as 'otherTeam'
    return: an array of Locaiton objects where you want to place your workers
    */
    getInitialPlacements(otherTeam:Location[]=null):Location[] {
        ///Place your logic here
        return null;    
    }

    /*
    given a state representation, return the next Turn object that your agent will take
    return: a Turn object
    */
    getNextTurn(currentState: State):Turn{
        //Place your logic here
        return null;  
    }
}

//example agents below
export class RandomAgent implements Agent{
    team:Team;
    constructor(team:Team){
        this.team = team;
    }
    getInitialPlacements(otherTeam:Location[]=null):Location[] {
        return [new Location(this.getRandomInt(25)), new Location(this.getRandomInt(25))];    
    }
    
    getNextTurn(currentState: State):Turn{
        
        let workers = currentState.getWorkersForTeam(this.team);
        let worker = workers[this.getRandomInt(workers.length)];
        let moveLocaitons = currentState.getPossibleMoveLocationsForWorker(worker);
        while(moveLocaitons.length==0){ //in case one worker doesn't have any moves
            worker = workers[this.getRandomInt(workers.length)];
            moveLocaitons = currentState.getPossibleMoveLocationsForWorker(worker);
        }
        let move = moveLocaitons[this.getRandomInt(moveLocaitons.length)];
        let possibleBuilds = [move-6, move-5, move-4,
            move-1,move+1,move+4, move+5, move+6].filter(i => i>-1 && i<25);
        let build = possibleBuilds[this.getRandomInt(possibleBuilds.length)];
        return new Turn(new Location(move), new Location(build), worker);    
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
}

//based off RandomAgent, will pick a high spot to move to, then build randomly
export class GoBigAgent implements Agent{ 
    team:Team;
    constructor(team:Team){
        this.team = team;
    }
    getInitialPlacements(otherTeam:Location[]=null):Location[] {
        return [new Location(this.getRandomInt(25)), new Location(this.getRandomInt(25))];    
    }
    
    getNextTurn(currentState: State):Turn{
        let workers = currentState.getWorkersForTeam(this.team);
        let move = null;
        let worker = workers[this.getRandomInt(workers.length)];
        while(move===null){
            worker = workers[this.getRandomInt(workers.length)];
            let moveLocaitons = currentState.getPossibleMoveLocationsForWorker(worker);
            // console.log(moveLocaitons, worker.location.boardIndex);
            // console.log(currentState.toString());
            move = this.getTallestValidSpot(moveLocaitons, currentState, worker);
        }
        let possibleBuilds = [move-6, move-5, move-4,
            move-1,move+1,move+4, move+5, move+6].filter(i => i>-1 && i<25);
        let build = possibleBuilds[this.getRandomInt(possibleBuilds.length)];
        return new Turn(new Location(move), new Location(build), worker);    
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    private getTallestSpot(boardIndexes:number[], state:State):number{
        if(boardIndexes.length<1) return null;    
        let tiles = boardIndexes.map(i=>state.board[i]);
        if(tiles.length<1) return null;
        tiles.sort((a,b)=> b.level - a.level);
        return tiles[0].location.boardIndex;
    }
    private getTallestValidSpot(boardIndexes:number[], state:State, worker:Worker):number{
        let move = this.getTallestSpot(boardIndexes, state);
        if (move===null) return null;
        while(!state.isMoveValid(new Move(worker, new Location(move)))){
            boardIndexes.splice(boardIndexes.findIndex(i=> i === move), 1);
            move = this.getTallestSpot(boardIndexes, state);
            if (move===null) return null;
        };
        return move;
    }
}


